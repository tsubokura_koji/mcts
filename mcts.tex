\documentclass[uplatex,a4paper,12pt]{jsarticle}
\usepackage[top=10truemm,bottom=20truemm,left=15truemm,right=15truemm]{geometry} % ページの余白を設定する
\usepackage[dvipdfmx]{graphicx}
\usepackage{multicol,amsmath,amssymb,here,algorithm,algpseudocode,comment,multirow}
\makeatletter
\def\@maketitle{% 
\begin{flushright}% 
{\large \@title \par}% タイトル 
{\large \@date \par}% 日付 
{\large \@author}% 著者 
\end{flushright}% 
\par\vskip 1.0em
}
\makeatother
\newcommand{\argmax}{\mathop{\rm arg~max}\limits}
\newcommand{\argmin}{\mathop{\rm arg~min}\limits}

\title{MCTS(モンテカルロ木探索)のまとめ}
\author{坪倉弘治}
\date{\today}

\begin{document}
\columnseprule=0.3mm
\maketitle
\begin{multicols}{2}
\newpage

\section{ゲーム木と探索木}
ゲーム木とは、ゲームの状態(盤面や局面ともいう)を有向グラフの節点、
ゲームの行動(手や着手ともいう)を枝で表したものである。
ゲーム木の説明としてよく用いられるのが図\ref{fig:sanmoku}に示した三目並べのゲーム木である。
(画像はwikiのゲーム木のページより、CC 表示-継承 3.0)

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=200.pt]{./img/sanmoku.png}
    \caption{三目並べのゲーム木}
    \label{fig:sanmoku}
  \end{center}
\end{figure}

それに対して探索木はゲーム木とは異なり、どのような手で現在の状態に到達したのかが重要である。
つまり、結果的に状態が同じになるとしても、取る手が異なれば探索木の節点も異なるということである。

取る手は異なるが結果的に状態が同じになる際に同じ節点に到達することを合流という。
ゲーム木は合流することがあるが、探索木は合流が発生しない。
ゲーム木で合流が発生する場合、構造が木ではなくDAG(Directed acyclic graph; 有向非巡回グラフ)
になってしまうが、そのままゲーム木と呼ばれることが多い。

探索木の節点には探索済み節点と未探索節点が存在し、
自身が未探索節点で親節点が探索済み節点である節点を先端節点と呼ぶ。
特に、節点数が膨大(もしくは無限)になる場合には、
実装上の探索木は探索済み節点と先端節点のみからなる部分グラフとして表現される。

1回の探索は次の手順で行う。
\begin{enumerate}
  \item 先端節点の集合$F$から次に探索する節点$i$を選択する。
  \item 節点$i$を$F$から取り除き探索を行う。
  \item 節点$i$の子節点を$F$に加える。(節点の展開)
\end{enumerate}
次に探索する節点の選択方法は探索アルゴリズムによって異なり、
先端節点の集合にスタックを用いるのが深さ優先探索、
キューを用いるのが幅優先探索となる。

\section{モンテカルロ法}
モンテカルロ法とは、乱数を用いたシミュレーションを繰り返すことで確率を近似的に求める手法である。
$n$回シミュレーションを行った時、ある事象$A$が$m$回起こったとすると
事象$A$が発生する確率$P(A)$は当然$\frac{m}{n}$で近似でき、
無限回シミュレーションを行うならば$\frac{m}{n}$は$P(A)$に収束する。
これがモンテカルロ法の基本となる考え方である。
ゲームAIの分野では、ゲームの状態の価値を推定するためにモンテカルロ法が用いられる。
また、次のセクションで説明するモンテカルロ木探索と区別するために、
原始モンテカルロ法と呼ばれることが多い。
原始モンテカルロ法の手続きをアルゴリズム\ref{alg:mc}に示す。
\begin{algorithm}[H]
  \caption{原始モンテカルロ法のアルゴリズム}
  \label{alg:mc}
  \begin{algorithmic}[1]
    \Require
      \Statex $s_t$:ゲームの状態
    \Function{MonteCarloSimulation}{$s_t$}
      \State $S_{t+1} \leftarrow s_t$で打つことのできる手$a_t$を打った時の次の状態の集合
      \ForAll{$s \in S_{t+1}$}
        \State $\Delta \leftarrow \sum^N$ \Call{RandomSimulation}{$s$}
        \State $x_s \leftarrow \frac{\Delta}{N}$
      \EndFor
      \State \Return $\argmax_{s \in S_{t+1}} x_s$
    \EndFunction
  \end{algorithmic}
\end{algorithm}
\textsc{RandomSimulation}関数は、ゲームの終端までランダムに着手し報酬を得る
ランダムシミュレーションを行う関数である。

\section{モンテカルロ木探索}
モンテカルロ木探索(Monte Carlo Tree Search; MCTS)は、モンテカルロ法に木探索の考えを取り入れた探索法であり、
2006年にR. Coulomによって提唱された。\cite{mcts}
モンテカルロ木探索は同氏が作成した囲碁プログラム「Crazy Stone\cite{mcts}」や「MoGo\cite{mogo}」などで使用されている。
モンテカルロ木探索で使用する探索木の節点はゲームの状態と根節点からの着手の系列を持つ。
つまり、節点の合流を考えず、同じ状態を持っていたとしても親節点が異なれば別の節点となる。
モンテカルロ木探索では、プレイアウトを繰り返し行い、節点の価値を近似的に求める。
プレイアウトには次の４段階がある。\cite{mctssurvey}
\begin{description}
  \item[木探索]何らかの方法で根節点$v_0$から展開していない節点(先端節点)$v_l$まで探索木を下る。
  \item[展開]$v_l$のプレイアウト回数が既定値を超えている場合に$v_l$を展開し、$v_l$の各子節点についてランダムシミュレーションとバックアップを行う。
  \item[ランダムシミュレーション]$v_l$の状態$s_l$からゲームの終端までランダムに着手し、$v_0$のターンプレイヤ$p$から見た報酬$\Delta$を得る。
  \item[バックアップ]ランダムシミュレーションで得られた結果を元に$v_l$とその先祖節点の値(プレイアウト回数や報酬和など)を更新する。
\end{description}
ここで$v_l$のプレイアウト回数とは、木探索における$v_l$の訪問回数のことである。
また、TUBSTAPにおいてランダムシミュレーションの報酬$\Delta$は、
ターンプレイヤ$p$から見て勝利の場合に1、敗北の場合に0、引き分けの場合に0.5とする。
節点$v$のプレイアウト回数を$N(v)$、節点$v$の報酬和を$R(v)$として、
モンテカルロ木探索の手続きをアルゴリズム\ref{alg:mcts}に示す。

\begin{algorithm}[H]
  \caption{モンテカルロ木探索のアルゴリズム}
  \label{alg:mcts}
  \begin{algorithmic}[1]
    \Require
      \Statex $s_0$:ゲームの状態
    \Function{MctsSearch}{$s_0$}
      \State 状態$s_0$で根節点$v_0$を作り探索木を構成する
      \State $p \leftarrow s_0$のターンプレイヤ
      \While{総プレイアウト回数が閾値未満}
        \State $ v_l \leftarrow$ \Call{TreePolicy}{$v_0$}
        \If{$v_l$のプレイアウト回数が既定値を超える}
          \State $v_l$を展開する
          \ForAll{$v_l$の子節点$v_{child}$}
            \State $ \Delta \leftarrow$ \Call{RandomSimulation}{$v_{child}, p$}
            \State \Call{Backup}{$v_{child}, \Delta$}
          \EndFor
        \Else
          \State $ \Delta \leftarrow$ \Call{RandomSimulation}{$v_l, p$}
          \State \Call{Backup}{$v_l, \Delta$}
        \EndIf
      \EndWhile
      \State \Return \Call{BestChildAct}{$v_0$}
    \EndFunction
    \Function{Backup}{$v, \Delta$}
      \Repeat
        \State $v \leftarrow v$の親節点
        \State $N(v) \leftarrow N(v) + 1$
        \State $R(v) + \Delta$
      \Until{$v$が根節点ではない}
    \EndFunction
    \Function{BestChildAct}{$v$}
      \State $max \leftarrow 0$
      \ForAll{$v$の子節点$v_{child}$}
        \If{$\frac{R(v_{child})}{N(v_{child})} > max$}
          \State $max = \frac{R(v_{child})}{N(v_{child})}$
          \State $a_{max} \leftarrow v_{child}$の着手
        \EndIf
      \EndFor
      \State \Return $a_{max}$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

アルゴリズム\ref{alg:mcts}の\textsc{TreePolicy}関数は、木探索で先端節点まで探索木を下る関数である。
また、\textsc{RandomSimulation}関数は、ランダムシミュレーションを行う関数である。
これら２つの関数はプログラムを実装する際に内部を定義する関数で、モンテカルロ木探索自体で決められた定義はない。
UCB1アルゴリズム\cite{ucb1}を木探索に適用したUCT(UCB applied to Trees)探索\cite{uct}では、
UCB値を利用して探索木を下る。
アルゴリズム\ref{alg:mcts}の\textsc{BestChildAct}関数における
$\frac{R(v_{child})}{N(v_{child})}$はプレイアウトの平均勝率である。
プレイアウトの平均勝率によって節点の価値を近似的に求め、プレイアウトの平均勝率が最も高い節点を選ぶことで、
節点の価値が最も高い節点を選択できると考えられる。

ここで節点の価値について、プレイアウトを無限回行ったと仮定すると、
節点の価値はミニマックス法のミニマックス値に収束する。
ミニマックス値を得るためにはミニマックス法の節点選択方策が分かればいい。
そしてそのためにはゲーム木の全探索が必要になる。
しかし、ターン制戦略ゲームでは複数着手性によって
全探索のために必要な計算時間が膨大になるので、全探索は行えない。
したがって、プレイアウトで節点の価値を推定する必要がある。

\section{UCB1アルゴリズム}
統計学や機械学習分野において多腕バンディット(Multi-Armed Bandit)問題という問題が存在する。
これは当たり易さに違いがあり、かつ当たり易さが未知の複数のスロットマシンに
どうコインを投入していけば報酬を大きくできるかという問題であり、
「より良いマシンを見つけるのにコインを使う」か
「分かっている中で最良のマシンでコインを使う」かのトレードオフが存在する。

この問題に対し、UCB1アルゴリズムが2002年に与えられた。\cite{ucb1}
UCBとはUpper Confidence Bound(信頼上限)の略であり、以下のように定義される。
\begin{equation}
ucb = \overline{x_j} + \sqrt{\frac{2 \log{n}}{n_j}} \nonumber
\end{equation}
　$ \overline{x_j} $はその時点での$j$番目のスロットマシンの報酬の平均を示し、
$n_j$はそのスロットマシンに投入されたコインの数、$n$は$n_j$の全スロットマシンについての和である。
これをUCB1値、または単にUCB値を呼ぶ。UCB値は(期待値)+(バイアス項)という構成であり、以下の考え方からなる。
\begin{itemize}
\item 期待値の高いところにより多くのコインを投入する。
\item コインの少ない場合は、単に運悪く実際より期待値が低い可能があるのでその分を考慮して優遇する。
\end{itemize}

UCB1アルゴリズムは以下のようなアルゴリズムである。
\begin{enumerate}
\item すべてのスロットマシンに1枚ずつコインを投入する。
\item 各スロットマシンについてUCB値を計算し、最大の値を持つスロットマシンにコインを投入する。
\item 制限時間が来るまで2.を繰り返す。
\end{enumerate}

\section{UCT探索}
UCB1アルゴリズムのUCB値をモンテカルロ木探索の「選択」で使用したのがUCT(UCB applied to Trees)探索である。
この探索法はKocsis, Levente and Szepesv{\'a}ri, Csabaらによって2006年に提唱された。\cite{uct}

このアルゴリズムで使用するUCB値は以下のように定義され、各節点がこの値を持つ。
\begin{equation}
ucb = \overline{x_j} + C \sqrt{\frac{2 \log{n}}{n_j}} \nonumber
\end{equation}

定数$C$は問題に合わせて調整するものとする。
報酬が$[0, 1]$の範囲に収まるのであれば理論的には$C=1$で良い。
また、式(2)の$n$は、節点$j$の親節点のプレイアウト回数である。

\section{M-UCT}
西野研に所属していた武藤孝輔が考案したターン制戦略ゲームの学術用基盤プロジェクトTUBSTAPのAIで用いられる手法である。
M-UCTとは、ターン制戦略ゲームにおける複数着手性(1ターンに全てのコマを動かすことができること)に着目し、
ゲームの行動aをそれぞれのユニットの行動(これをユニット行動という)の列$a=(ua_1,\cdots,ua_n)$からなると考え、
探索木の節点を行動に対応させるのではなくユニット行動に対応させたユニット行動木でUCT探索を行う探索法である。
これによって、もともと1段で表された自分ターンがユニット数分の深さで表されるようになり、
その分探索木の横幅が狭くなる。

\bibliography{ref} %ref.bibから拡張子を外した名前
\bibliographystyle{junsrt} %参考文献出力スタイル
\end{multicols}
\end{document}